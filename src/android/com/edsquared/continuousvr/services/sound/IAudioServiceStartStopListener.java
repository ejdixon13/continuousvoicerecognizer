package com.edsquared.continuousvr.services.sound;

public interface IAudioServiceStartStopListener {

    public void onAudioServiceStateChange();
}
