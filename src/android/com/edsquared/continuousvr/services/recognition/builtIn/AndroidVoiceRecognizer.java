package com.edsquared.continuousvr.services.recognition.builtIn;

import org.json.JSONArray;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;
import org.json.JSONException;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
//import android.os.VibrationEffect;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

import com.edsquared.continuousvr.services.recognition.AbstractRecognizer;
import com.edsquared.continuousvr.services.sound.AudioService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class AndroidVoiceRecognizer extends AbstractRecognizer {

    private static final String TAG = "AndroidVoiceRecService";
    private static final String BLUETOOTH_TAG = "BTVoiceRecService";

    private SpeechRecognizer speech = null; // Speech recognizer instance!
    private AudioManager audioManager;
    private boolean beepOff = false;
    private RecognitionListener recognitionListener;
    private CallbackContext context;
    private AudioService audioService;
    private int maxMatches;
    private static int mStreamVolume = 0;
    private String lastActionTaken = "";
    private static final double CONFIDENCE_THRESHOLD = .80;

    private BluetoothVoiceRecognition bluetoothVoiceRecognition;

// ... call functions on mBluetoothHeadset


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) {
        Boolean isValidAction = true;
        this.context = context;
        if(audioService == null) {
            this.audioService = new AudioService();
        }

        try {
            if (args.length() > 0) {
                String temp = args.getString(0);
                maxMatches = Integer.parseInt(temp);
            }
        }
        catch (Exception e) {
            Log.e(TAG, String.format("startSpeechRecognitionActivity exception: %s", e.toString()));
        }
        if ("startRecognize".equals(action)) {
            initialize();
            start();
        } else if("stopRecognize".equals(action)){
            stop();
        } else if ("getSupportedLanguages".equals(action)) {
//            getSupportedLanguages();
        } else if("setPlaybackVolume".equals(action)) {
            try {
                Log.i(TAG, "args: " + args.toString());
                int newVolume = (Integer)args.get(0);
                if(newVolume > 0) {
                    mStreamVolume = newVolume;
                }
                this.setPlaybackVolume(mStreamVolume, false, 200);
                this.context.success(mStreamVolume);
            } catch (JSONException e) {
                Log.e(TAG, String.format("setPlaybackVolume exception: %s", e.toString()));
            }

        }
        else {
            this.context.error("Unknown action: " + action);
            isValidAction = false;
        }

        lastActionTaken = action;
        return isValidAction;
    }


    @Override
    public void initialize() {
        Toast.makeText(cordova.getActivity().getBaseContext(), "Voice Recognition Initialization", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "initialize called");
        audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
        saveCurrentVolume();

        recognitionListener = new AbstractAndroidRecognitionListener() {

            @Override
            public void onError(int error) {
                super.onError(error);

                setStatus(translateError(error) + "!");
                Log.i(TAG, "Error: " + error + " " + translateError(error));
                Log.i(TAG, "Restarting Android Speech Recognizer");
                if (super.restartWhenError(error)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.i(TAG, "Volume onError restart: " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                            saveCurrentVolume();
                            turnBeepOff();
                            Log.i(TAG, "Volume onError restart after beep off: " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                            listenAgain();
                        }
                    },100);
                    setStatus("restart");
                }
            }

            private void listenAgain()
            {
                Log.i(TAG, "Listen Again Called");
                getSpeechRecognizer().cancel();
                startVoiceRecognitionCycle();
            }

            @Override
            public void onResults(Bundle results) {
                setStatus("got results, restart");

                cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        listenAgain();
                    }
                });// Restart new dictation cycle

                StringBuilder scores = new StringBuilder();
                for (int i = 0; i < results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES).length; i++) {
                    scores.append(results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES)[i] + " ");
                }
                Log.d(TAG, "onResults: " + results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                        + " scores: " + scores.toString());

//                TODO: Eventually, when more recogizers are present we will average or take the highest scored
                boolean passedConfidenceThreshold = results.getFloatArray(SpeechRecognizer.CONFIDENCE_SCORES)[0] > CONFIDENCE_THRESHOLD;
                // Add results and notify
                if (results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION) != null) {
                    String chunk = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION).get(0);
                    if (chunk != null) {
//                        addWords(chunk);

                        ArrayList<String> matches = new ArrayList<String>();
                        ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                        for (int i = 0; i < data.size(); i++) {
                            matches.add((String) data.get(i));
                        }
                        if(passedConfidenceThreshold || containsExceptions(matches)){
                            triggerVibration();
                            returnProgressResults(matches);
                        } else {
                            Log.d(TAG, ": Double Vibration Triggered!");
                            triggerDoubleVibration();
                            returnRetryVoiceCommandError();
                        }

                    }
                }
            }



            @Override
            public void onReadyForSpeech(Bundle params) {
                Log.d(TAG, "onReadyForSpeech");
            }
        };
    }

    private void saveCurrentVolume() {
        Log.i(TAG, "saveCurrentVolume - old volume: " + mStreamVolume);
        int newVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        Log.i(TAG, "saveCurrentVolume - new volume: " + mStreamVolume);
        Log.i(TAG, "onReadyForSpeech");
        if(newVolume > 0) {
            mStreamVolume = newVolume;
        }
    }

    private boolean containsExceptions(List<String> matches) {
        ArrayList<String> exceptions = new ArrayList<String>(Arrays.asList(
                ",",
                ".",
                "?",
                "!"
        ));
        for(String exception : exceptions) {
            if(matches.contains(exception)) {
                return true;
            }
        }
        return false;
    }

    private void returnProgressResults(ArrayList<String> matches) {
        Log.d(TAG, "returnProgressResults called");
        JSONArray jsonMatches = new JSONArray(matches);

        PluginResult progressResult = new PluginResult(PluginResult.Status.OK, jsonMatches);
        progressResult.setKeepCallback(true);
        context.sendPluginResult(progressResult);
    }

    private void returnRetryVoiceCommandError() {
        Log.d(TAG, "retryVoiceCommandError");
        PluginResult progressResult = new PluginResult(PluginResult.Status.ERROR, "RETRY VOICE COMMAND");
        progressResult.setKeepCallback(true);
        context.sendPluginResult(progressResult);
    }


    // Service control ->
    @Override
    public void start() {
        super.start();

        Log.i(TAG, "start() called");

        if (audioService.isRunning()) {
            Log.i(TAG, "Turning AudioService off");
            audioService.shutdown();
            setStatus("AudioService turn off");
        }

        saveCurrentVolume();
        turnBeepOff();
        this.bluetoothVoiceRecognition = new BluetoothVoiceRecognition();
        this.bluetoothVoiceRecognition.initializeVoiceRecognition(cordova.getActivity().getBaseContext());
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                startVoiceRecognitionCycle();
            }
        });
        setStatus("started");
    }

    @Override
    public void stop() {
        super.stop();
        if(this.bluetoothVoiceRecognition != null) {
            this.bluetoothVoiceRecognition.stopVoiceRecognition();
        }

        Log.i(TAG, "stop() called");
        if (speech != null) {
            speech.destroy();
            speech = null;
        }
        turnBeepOn();

        returnProgressResults(new ArrayList<String>());

        if (!audioService.isRunning()) {
            Log.i(TAG, "Turning AudioService on");
            audioService.initialize();
            setStatus("AudioService turn on");
        }
    }

    /**
     * Destroy the recognizer.
     */
    @Override
    public void reset() {
        super.reset();
    }

    @Override
    public void shutdown() {
        super.shutdown();
        Log.i(TAG, "shutdown() called");

        if (speech != null) {
            speech.destroy();
            speech = null;
        }
    }

    /**
     * Lazy instantiation method for getting the speech recognizer
     *
     * @return the android speech recognizer
     */
    private SpeechRecognizer getSpeechRecognizer() {
        if (speech == null) {
            speech = SpeechRecognizer.createSpeechRecognizer(cordova.getActivity().getBaseContext());
            speech.setRecognitionListener(recognitionListener);
        }

        return speech;
    }

    /**
     * Fire an intent to start the voice recognition process.
     */
    public void startVoiceRecognitionCycle() {
        Log.d(TAG, "startVoiceRecognitionCycle called");
        if (running) {

            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, cordova.getActivity().getBaseContext().getPackageName());
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, settings.getLanguage().getCode4());
//            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US");
            if (maxMatches > 0) {
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, maxMatches);
            }
            getSpeechRecognizer().startListening(intent);
            setStatus("listening");
        }
    }

    private void turnBeepOff() {
        Log.d(TAG, "Beep turned off");
        audioManager.setMode(AudioManager.MODE_IN_CALL);
        this.setPlaybackVolume(0, true, 400);
        beepOff = true;
    }

    private void turnBeepOn() {
        Log.d(TAG, "Beep turned on with volume: " + mStreamVolume);
        if (beepOff) {
            this.setPlaybackVolume(mStreamVolume, false, 200);
            beepOff = false;
        }
    }

    private void setPlaybackVolume(final Integer volume, Boolean streamSolo, Integer delay) {
        audioManager.setStreamSolo(AudioManager.STREAM_VOICE_CALL, streamSolo);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
                }
            }, delay);
    }

    private void triggerVibration() {
//        if (Build.VERSION.SDK_INT >= 26) {
//            //TODO: VibrationEffect not recognized. Maybe I need to upgrade libraries?
//            ((Vibrator) cordova.getActivity().getBaseContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(150, VibrationEffect.DEFAULT_AMPLITUDE));
//        } else {
            ((Vibrator) cordova.getActivity().getBaseContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(200);

//        }

    }

    private void triggerDoubleVibration() {
        long[] twice = { 0, 100, 200, 100 };
//        if (Build.VERSION.SDK_INT >= 26) {
//            //TODO: VibrationEffect not recognized. Maybe I need to upgrade libraries?
//            ((Vibrator) cordova.getActivity().getBaseContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(150, VibrationEffect.DEFAULT_AMPLITUDE));
//        } else {
            ((Vibrator) cordova.getActivity().getBaseContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(twice, -1);

//        }


    }



    @Override
    public void onResume(boolean multitasking) {
        Log.e(TAG, String.format("SpeechWriter: OnResume : %s", "Speech is null: " + (speech == null)));

        if(this.context != null){
            if(audioService == null) {
                this.audioService = new AudioService();
            }
            if ("startRecognize".equals(lastActionTaken)) {
                if(speech == null) {
                    initialize();
                    start();
                }
            }
        }
    }

    @Override
    public void onStop() {
        Log.e(TAG, String.format("SpeechWriter: OnStop : %s", "CALLED"));
        if(this.context != null) {
            stop();
            reset();
            audioService.initialize();
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, String.format("SpeechWriter: OnDestroy : %s", "CALLED"));

        if(this.context != null) {
            stop();
            reset();
        }
    }

    @Override
    public void onReset() {
        Log.e(TAG, String.format("SpeechWriter: OnReset : %s", "CALLED"));

        if(this.context != null) {
            stop();
            reset();
        }
    }

    @Override
    public String getName() {
        return "Android Recognizer";
    }

    public class BluetoothVoiceRecognition {
        private BluetoothAdapter bluetoothAdapter;
        private Set<BluetoothDevice> pairedDevices;
        private BluetoothProfile.ServiceListener mProfileListener;
        private BluetoothHeadset bluetoothHeadset;
        private Boolean bluetoothVoiceRecognitionActive = false;


        public void initializeVoiceRecognition(final Context context) {
//            Toast.makeText(cordova.getActivity().getBaseContext(), "BT Init", Toast.LENGTH_SHORT).show();

            if(bluetoothAdapter == null || mProfileListener == null) {
//                Toast.makeText(cordova.getActivity().getBaseContext(), "BT no adapter or listener", Toast.LENGTH_SHORT).show();

                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                if(!bluetoothAdapter.isEnabled()) { return; } // ensure bluetooth is on

                pairedDevices = bluetoothAdapter.getBondedDevices();
                final List<String> pairedDeviceAddresses = new ArrayList<String>();
                for(BluetoothDevice device : pairedDevices) {
                    pairedDeviceAddresses.add(device.getAddress());
                }
                Log.i(BLUETOOTH_TAG, "Initialized bluetooth voice rec with the following paired devices: " + pairedDeviceAddresses.toString());


                mProfileListener = new BluetoothProfile.ServiceListener() {
                    public void onServiceConnected(int profile, BluetoothProfile proxy)
                    {
                        Log.i(BLUETOOTH_TAG, "Bluetooth On Service Connected");
//                        Toast.makeText(cordova.getActivity().getBaseContext(), "BT Service Connected", Toast.LENGTH_SHORT).show();

                        if (profile == BluetoothProfile.HEADSET && !pairedDevices.isEmpty() && proxy != null)
                        {
//                            Toast.makeText(context, "BluetoothHeadset Proxy set", Toast.LENGTH_SHORT).show();
                            Log.i(BLUETOOTH_TAG, "bluetoothHeadset proxy set");
                            bluetoothHeadset = (BluetoothHeadset) proxy;
                            startVoiceRecognition();
                        }
                    }
                    public void onServiceDisconnected(int profile)
                    {
                        Log.i(BLUETOOTH_TAG, "Bluetooth on Service Disconnected");
//                        Toast.makeText(context, "BluetoothHeadset proxy Disconnected", Toast.LENGTH_SHORT).show();
                        if (profile == BluetoothProfile.HEADSET && !bluetoothVoiceRecognitionActive) {
                            Log.i(TAG, "bluetoothHeadset proxy set to null");
                            bluetoothHeadset = null;
                        } else {
                            startVoiceRecognition();
                        }
                    }
                };
                bluetoothAdapter.getProfileProxy(context, mProfileListener, BluetoothProfile.HEADSET);
            } else {
                startVoiceRecognition();
            }
        }

        //false if there is no headset connected, or the connected headset doesn't support voice recognition,
        // or voice recognition is already started, or audio channel is occupied, or on error, true otherwise
        public boolean startVoiceRecognition() {
            boolean voiceRecognitonStartResult = false;
            if(bluetoothHeadset != null) {
                Log.i(BLUETOOTH_TAG, "Starting Bluetooth Voice Recognition");

                if (bluetoothAdapter.isEnabled()) {
                    Log.i(BLUETOOTH_TAG, "Bluetooth Adapter Enabled");

                    for (BluetoothDevice tryDevice : pairedDevices) {
                        Log.i(BLUETOOTH_TAG, "Attempting to start bluetooth voice recognition on device: " + tryDevice.getAddress());
                        voiceRecognitonStartResult = bluetoothHeadset.startVoiceRecognition(tryDevice);
                        Log.i(BLUETOOTH_TAG, "Start bluetooth Voice recognition result for device " + tryDevice.getAddress() + ": " + voiceRecognitonStartResult);
                        //This loop tries to start VoiceRecognition mode on every paired device until it finds one that works(which will be the currently in use bluetooth headset)
                        if (voiceRecognitonStartResult) {
                            Toast.makeText(cordova.getActivity().getBaseContext(), "Bluetooth Voice Recognition Started", Toast.LENGTH_SHORT).show();
                            bluetoothVoiceRecognitionActive = true;
                            break;
                        }
                    }
                    if(bluetoothVoiceRecognitionActive) {
                        boolean bluetoothScoOn = audioManager.isBluetoothScoOn();
                        if(!bluetoothScoOn) {
                            audioManager.setSpeakerphoneOn(false);
                            audioManager.setBluetoothScoOn(true);
                            audioManager.startBluetoothSco();
                            audioManager.setMode(AudioManager.MODE_IN_CALL);
                        }
                        Log.i(BLUETOOTH_TAG, "Is bluetooth SCO on: " + bluetoothScoOn);
                    }
                }
                Log.i(BLUETOOTH_TAG, "Final start voice recognition result: " + voiceRecognitonStartResult);
            }

            return voiceRecognitonStartResult;
        }

        public boolean stopVoiceRecognition(){
            boolean voiceRecognitonStopResult = false;
            if(bluetoothHeadset != null) {
                Log.i(BLUETOOTH_TAG, "Stopping bluetooth voice recognition");

                if(bluetoothAdapter != null) {
                    if (bluetoothAdapter.isEnabled()) {
                        Log.i(BLUETOOTH_TAG, "Bluetooth Adapter Enabled");

                        for (BluetoothDevice tryDevice : pairedDevices) {
                            Log.i(BLUETOOTH_TAG, "Attempting to start bluetooth voice recognition on device: " + tryDevice.getAddress());
                            voiceRecognitonStopResult = bluetoothHeadset.stopVoiceRecognition(tryDevice);
                            Log.i(BLUETOOTH_TAG, "Stop Bluetooth Voice recognition result for device " + tryDevice.getAddress() + ": " + voiceRecognitonStopResult);
                            //This loop tries to start VoiceRecognition mode on every paired device until it finds one that works(which will be the currently in use bluetooth headset)
                            if (voiceRecognitonStopResult) {
                                Toast.makeText(cordova.getActivity().getBaseContext(), "Bluetooth Voice Recognition Stopped", Toast.LENGTH_SHORT).show();
                                bluetoothVoiceRecognitionActive = false;
                                break;
                            }
                        }
                    }
                    bluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset);
                    bluetoothAdapter = null;
                }
            }
            return voiceRecognitonStopResult;
        }
    }

}
