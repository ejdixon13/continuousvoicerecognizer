package com.edsquared.continuousvr.services.recognition;

public interface ITranscriptionResultListener {

    public void onTranscriptResult(String transcriptResult);
}
