package com.edsquared.continuousvr.services.recognition;

public interface IStatusListener {

    public void onStatusUpdate(String newStatus);
}
