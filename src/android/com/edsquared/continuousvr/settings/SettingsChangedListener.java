package com.edsquared.continuousvr.settings;

public interface SettingsChangedListener {
    
    public void settingChanged();

}
