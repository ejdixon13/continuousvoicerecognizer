## Android Voice Transcription Plugin for Ionic/Cordova

This plugin extends the voice transcription capabilities on Android beyond the default system limits. Android typically stops transcription after a short period, but with this plugin, your Ionic/Cordova apps can keep the transcription going indefinitely.
Features:


-    Seamless Integration: Drop it into your Ionic/Cordova project to bypass Android's voice transcription time limits.
-    Continuous Transcription: Overrides system defaults to allow longer voice input without interruptions.
-    Automatic Restart: Detects when the system ends transcription and restarts it, ensuring a smooth user experience.